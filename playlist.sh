#!/bin/bash

# this script will generate all the youtube links included in the given playlist URL
# just give it a playlist link and it'll generate a file called youtube-playlist-XXXXXXXXXX.list
# usage : ./generate_links.sh 'https://www.youtube.com/watch?v=WUvTyaaNkzM&list=PLZHQObOWTQDMsr9K-rj53DwVRMYO3t5Yr'
# notice that quotes are needed, for now.


# extract indexes into a file located at $INDEXES
function extract_indexes {
	cat $HTML 	| grep '<li\ class="yt-uix-scroller-scroll-unit' \
			| grep -oE 'data-index="[0-9]*"' \
			| cut -d'"' -f2 \
			| cut -d'"' -f1 > $INDEXES
}

# extract ids into a file located at $IDS
function extract_ids {
	cat $HTML       | grep '<li\ class="yt-uix-scroller-scroll-unit' \
        	        | grep -Eo 'data-video-id="..........."' \
                	| cut -d'"' -f2 \
	                | cut -d'"' -f1 > $IDS
}

PREFIX='https://www.youtube.com/watch?v='
FILE_NAME=$(date +'%Y%m%d.%H%M%S')

HTML='/tmp/'$FILE_NAME'.html'
INDEXES='/tmp/'$FILE_NAME'.index'
IDS='/tmp/'$FILE_NAME'.id'

LIST_ID=$(echo $1 | cut -d'&' -f2)
curl $1 1> $HTML 2> /dev/null

# extract indexes and ids :
extract_indexes
extract_ids

LENGTH=$(cat $INDEXES | wc -l)
LINKS='./youtube-playlist-'$FILE_NAME'.list'
touch $LINKS

for i in $(seq 1 $LENGTH)
do
	#URL_INDEX=$(sed $i'!d' $INDEXES | sed 's/^/\&index=/')
	URL_ID=$(sed $i'!d' $IDS)
	RESULT=$PREFIX$URL_ID	#'&'$LIST_ID$URL_INDEX
	echo $RESULT >> $LINKS
done

#Remove temp files
rm $HTML
rm $INDEXES
rm $IDS

echo 'all done. the playlist links are located at' $LINKS
